package com.veryindyprogrammers.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.veryindyprogrammers.SabiokaMain;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		System.setProperty("user.name","EnglishWords");
		config.width=1334;
		config.height=710;
		new LwjglApplication(SabiokaMain.getInstance(), config);
	}
}

package com.veryindyprogrammers.View;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.veryindyprogrammers.Model.LevelWorld;
import com.veryindyprogrammers.SabiokaMain;

/**
 * Created by KirillovDaniel on 11.05.2016.
 */
public class HealthScale extends Group {
    private ImageActor[] hearts;
    private LevelWorld stage;

    public HealthScale(TextureRegion heart, float x, float y, LevelWorld stage) {
        this.stage=stage;
        hearts = new ImageActor[(int) (stage.getPlayer().getHp() / 1)];
        System.out.println("_HEALTH%10__" + stage.getPlayer().getHp() / 1);
        for (int i = 0; i < stage.getPlayer().getHp() /1; i++) {
            hearts[i] = new ImageActor(heart, x + i * (SabiokaMain.OUTPUT_TILE_SIZE + 0.18f), y, SabiokaMain.OUTPUT_TILE_SIZE+0.08f, SabiokaMain.OUTPUT_TILE_SIZE+0.08f);
        }
    }
    public void draw(Batch batch, float parentAlpha){
        System.out.println("HEALTH_(inHealthScale)__" + stage.getPlayer().getHp());
        for(int i=0;i<stage.getPlayer().getHp()/1;i++)
            hearts[i].draw(batch,parentAlpha);
    }
}

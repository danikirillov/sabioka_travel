package com.veryindyprogrammers.Controllers;

/**
 * Created by 04k1203 on 16.05.2016.
 */
public enum GameState {
    RUNNING,
    PAUSE
}

package com.veryindyprogrammers.Model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.veryindyprogrammers.SabiokaMain;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.KinematicBody;
import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.StaticBody;

/**
 * Created by KirillovDaniel on 08.04.2016.
 */
public class Box extends Actor {
    private TextureRegion img;
    private Body body;
    private String tile;
    private long stTime = 0;
    private long intervalTime = 0;
    private TextureRegion hideBlock;

    public Box(TextureRegion img, float x, float y, World world, String tile) {
        this.img = img;
        setPosition(x, y);
        setSize(img.getRegionWidth(), img.getRegionHeight());
        hideBlock = SabiokaMain.getInstance().getTextures().get("fullStR");
        this.tile = tile;
        BodyDef bodyDef = new BodyDef();
        bodyDef.fixedRotation = true;
        bodyDef.position.x = x;
        bodyDef.position.y = y;
        if (tile.equals("i") || tile.equals("ii"))
            bodyDef.type = KinematicBody;
        else
            bodyDef.type = StaticBody;
        body = world.createBody(bodyDef);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = new PolygonShape();
        switch (tile.charAt(0)) {
            case '7':
                ((PolygonShape) fixtureDef.shape).setAsBox(SabiokaMain.OUTPUT_TILE_SIZE / 2, SabiokaMain.OUTPUT_TILE_SIZE / 4, new Vector2(0, SabiokaMain.OUTPUT_TILE_SIZE / 4), 0f);
                break;
            case 'i':
                ((PolygonShape) fixtureDef.shape).setAsBox(SabiokaMain.OUTPUT_TILE_SIZE / 2, SabiokaMain.OUTPUT_TILE_SIZE / 4, new Vector2(0, SabiokaMain.OUTPUT_TILE_SIZE / 4), 0f);
                break;
            case '3':
                ((PolygonShape) fixtureDef.shape).set(new Vector2[]{
                        new Vector2(-SabiokaMain.OUTPUT_TILE_SIZE / 2, -SabiokaMain.OUTPUT_TILE_SIZE / 2),
                        new Vector2(-SabiokaMain.OUTPUT_TILE_SIZE / 2, SabiokaMain.OUTPUT_TILE_SIZE / 2),
                        new Vector2(SabiokaMain.OUTPUT_TILE_SIZE / 2, -SabiokaMain.OUTPUT_TILE_SIZE / 2),
                        new Vector2(-SabiokaMain.OUTPUT_TILE_SIZE / 2, -SabiokaMain.OUTPUT_TILE_SIZE / 2)
                });
                break;
            case '4':
                ((PolygonShape) fixtureDef.shape).set(new Vector2[]{
                        new Vector2(-SabiokaMain.OUTPUT_TILE_SIZE / 2, -SabiokaMain.OUTPUT_TILE_SIZE / 2),
                        new Vector2(SabiokaMain.OUTPUT_TILE_SIZE / 2, SabiokaMain.OUTPUT_TILE_SIZE / 2),
                        new Vector2(SabiokaMain.OUTPUT_TILE_SIZE / 2, -SabiokaMain.OUTPUT_TILE_SIZE / 2),
                        new Vector2(-SabiokaMain.OUTPUT_TILE_SIZE / 2, -SabiokaMain.OUTPUT_TILE_SIZE / 2)
                });
                break;
            case 'a':
                ((PolygonShape) fixtureDef.shape).set(new Vector2[]{
                        new Vector2(-SabiokaMain.OUTPUT_TILE_SIZE / 2, -SabiokaMain.OUTPUT_TILE_SIZE / 2),
                        new Vector2(-SabiokaMain.OUTPUT_TILE_SIZE / 2, SabiokaMain.OUTPUT_TILE_SIZE / 2),
                        new Vector2(SabiokaMain.OUTPUT_TILE_SIZE / 2, SabiokaMain.OUTPUT_TILE_SIZE / 2),
                        new Vector2(-SabiokaMain.OUTPUT_TILE_SIZE / 2, -SabiokaMain.OUTPUT_TILE_SIZE / 2)
                });
                break;
            case 'b':
                ((PolygonShape) fixtureDef.shape).set(new Vector2[]{
                        new Vector2(-SabiokaMain.OUTPUT_TILE_SIZE / 2, SabiokaMain.OUTPUT_TILE_SIZE / 2),
                        new Vector2(SabiokaMain.OUTPUT_TILE_SIZE / 2, SabiokaMain.OUTPUT_TILE_SIZE / 2),
                        new Vector2(SabiokaMain.OUTPUT_TILE_SIZE / 2, -SabiokaMain.OUTPUT_TILE_SIZE / 2),
                        new Vector2(-SabiokaMain.OUTPUT_TILE_SIZE / 2, SabiokaMain.OUTPUT_TILE_SIZE / 2),
                });
                break;
            case 'p':
                ((PolygonShape) fixtureDef.shape).setAsBox(SabiokaMain.OUTPUT_TILE_SIZE / 6, SabiokaMain.OUTPUT_TILE_SIZE / 2);
                break;
            case 't':
                ((PolygonShape) fixtureDef.shape).setAsBox(SabiokaMain.OUTPUT_TILE_SIZE / 2, SabiokaMain.OUTPUT_TILE_SIZE / 8, new Vector2(0, -SabiokaMain.OUTPUT_TILE_SIZE / 3), 0f);
                break;
            default:
                ((PolygonShape) fixtureDef.shape).setAsBox(SabiokaMain.OUTPUT_TILE_SIZE / 2, SabiokaMain.OUTPUT_TILE_SIZE / 2);
        }
        System.out.println("_BoxSizeX___" + SabiokaMain.OUTPUT_TILE_SIZE);
        System.out.println("_BoxSizeY___" + SabiokaMain.OUTPUT_TILE_SIZE);
        System.out.println("_BoxPosition(x,y)___" + body.getPosition().x + "," + body.getPosition().y);
        fixtureDef.friction = 0f;
        fixtureDef.density = 0f;
        fixtureDef.restitution = 0f;
        body.createFixture(fixtureDef);
        body.getFixtureList().get(0).setUserData(tile);
        fixtureDef.shape.dispose();
    }

    public void act(float delta) {
        setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().y - getHeight() / 2);
    }

    public void draw(Batch batch, float parentAlpha) {
        if (tile.length() > 1 && tile.charAt(1) == 'i') {
            intervalTime = System.nanoTime() - stTime;
            if (intervalTime / 1000000000 > 1.5 || intervalTime == 0 || stTime == 0) {
                stTime = System.nanoTime();
            } else {
                if (intervalTime / 1000000000 < 1) {
                    batch.draw(img, getX(), getY(), getWidth(), getHeight());
                }
            }
        } else {
            if (tile.charAt(0) == '8') {
                if (SabiokaMain.getInstance().isNear())
                    batch.draw(img, getX(), getY(), getWidth(), getHeight());
            } else {
                if(tile.length() > 1 &&tile.charAt(1)=='h')
                    batch.draw(hideBlock,getX(),getY(),getWidth(),getHeight());
                else
                    batch.draw(img, getX(), getY(), getWidth(), getHeight());
            }
        }


    }
}


package com.veryindyprogrammers.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.veryindyprogrammers.SabiokaMain;
import com.veryindyprogrammers.View.ImageActor;

import java.util.HashMap;

/**
 * Created by KirillovDaniel on 19.03.2016.
 */
public class HighscoreScreen implements Screen {
    private Stage stage;
    private ImageActor backGround;
    private ImageActor btnBack;

    public HighscoreScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions){
        btnBack= new ImageActor(textureRegions.get("Back button"), 13f, 2f, 1.2f,1.2f);
        btnBack.addListener(new ClickListener(){public void clicked(InputEvent event, float x,float y){SabiokaMain.getInstance().showMenu();}});
        backGround = new ImageActor(textureRegions.get("Black color"),0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        OrthographicCamera camera=new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage= new Stage(new ScreenViewport(camera),batch);
        stage.addActor(backGround);
        stage.addActor(btnBack);
    }
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}


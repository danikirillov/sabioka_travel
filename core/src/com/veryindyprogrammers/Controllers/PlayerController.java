package com.veryindyprogrammers.Controllers;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.veryindyprogrammers.Model.LevelWorld;
import com.veryindyprogrammers.SabiokaMain;


/**
 * Created by KirillovDaniel on 22.04.2016.
 */
public class PlayerController implements InputProcessor {
    private Stage stage;
    private LevelWorld levelWorld;
    private long startTime, intervalTime;
    private long startTime1 = 0;
    private long intervalTime1;
    private int doubleTap = 0;
    private boolean flip = false;

    public PlayerController(Stage stage, LevelWorld levelWorld) {
        this.stage = stage;
        this.levelWorld = levelWorld;
    }

    public boolean touchDown(int screenX, int screenY, int pointer, int btn) {
        Actor actor = levelWorld.hit(screenX, screenY, true);
        String aName;
        if (actor == null) {
            //проверка двойного касания
            if (doubleTap == 0) {
                startTime = System.nanoTime();
            }
            doubleTap++;
            intervalTime = System.nanoTime() - startTime;
            if (intervalTime <= 1000000000 && doubleTap == 2) {
                if (SabiokaMain.getInstance().getDirection() == Direction.RIGHT)
                    SabiokaMain.getInstance().setDirection(Direction.LEFT);
                else
                    SabiokaMain.getInstance().setDirection(Direction.RIGHT);
                flip = true;
            }
            if (doubleTap >= 2) doubleTap = 0;
            return false;
        } else {
            aName = actor.getName();
            if (SabiokaMain.getInstance().getPlayerAction() == PlayerAction.NONE && aName != null) {
                if (aName.equals("btnDown")) {
                    SabiokaMain.getInstance().setPlayerAction(PlayerAction.FALL);
                }
                if (aName.equals("btnUp")) {
                    SabiokaMain.getInstance().setPlayerAction(PlayerAction.JUMP);
                }
                if (aName.equals("btnPause")) {
                    if (SabiokaMain.getInstance().getState() == GameState.RUNNING)
                        SabiokaMain.getInstance().setState(GameState.PAUSE);
                    else
                        SabiokaMain.getInstance().setState(GameState.RUNNING);
                }
            }
        }
        System.out.println("MOVE_clicked___" + aName);
        return true;
    }

    public boolean touchUp(int screenX, int screenY, int pointer, int btn) {
        SabiokaMain.getInstance().setPlayerAction(PlayerAction.NONE);
        return true;
    }

    public void update() {
        if (levelWorld.getPlayer().getHp() <= 0) {
            levelWorld.getPlayer().hpUp(5);
            SabiokaMain.getInstance().showGameOverScreen();
        } else {
            if (SabiokaMain.getInstance().getPlayerAction() != null) {
                if (SabiokaMain.getInstance().getDirection() == Direction.RIGHT) {
                    levelWorld.getPlayer().getBody().setLinearVelocity(2f, levelWorld.getPlayer().getBody().getLinearVelocity().y);
                } else {
                    levelWorld.getPlayer().getBody().setLinearVelocity(-2f, levelWorld.getPlayer().getBody().getLinearVelocity().y);
                }

                if (SabiokaMain.getInstance().isSlopeDown())
                    levelWorld.getPlayer().setAngle(-45f);
                else {
                    if (SabiokaMain.getInstance().isSlopeUp())
                        levelWorld.getPlayer().setAngle(45f);
                    else
                        levelWorld.getPlayer().setAngle(0f);
                }
                if (flip) {
                  //  levelWorld.getPlayer().getImg().flip(true, false);
                    flip = false;
                }
                switch (SabiokaMain.getInstance().getPlayerAction()) {
                    case JUMP:
                        if (SabiokaMain.getInstance().isGrounded()) {
                            levelWorld.getPlayer().getBody().setLinearVelocity(levelWorld.getPlayer().getBody().getLinearVelocity().x, 3f);
                            SabiokaMain.getInstance().setIsGrounded(false);
                            System.out.println("ISGROUNDED_(inUpdate)_state_" + SabiokaMain.getInstance().isGrounded());
                            SabiokaMain.getInstance().setIsSlopeDown(false);
                            SabiokaMain.getInstance().setIsSlopeUp(false);
                        }
                        break;
                    case FALL:
                        if (!SabiokaMain.getInstance().isGrounded()) {
                            levelWorld.getPlayer().getBody().setLinearVelocity(levelWorld.getPlayer().getBody().getLinearVelocity().x, -2f);
                        }
                        break;
                    case STAND:
                        levelWorld.getPlayer().getBody().applyLinearImpulse(new Vector2(0f, 0f), new Vector2(levelWorld.getPlayer().getX(), levelWorld.getPlayer().getY()), true);
                        break;
                }

            }
            if (SabiokaMain.getInstance().isPortal()) {
                levelWorld.getPlayer().getBody().setTransform(2f, 2.5f, 0);
                SabiokaMain.getInstance().setPortal(false);
                levelWorld.getPlayer().hpDown(1);
                System.out.println("Player_HP__" + levelWorld.getPlayer().getHp());
            }

            intervalTime1 = System.nanoTime() - startTime1;
            if ((SabiokaMain.getInstance().isInWater() || SabiokaMain.getInstance().isOnPins()) && intervalTime1 / 100000000 > 5) {
                startTime1 = System.nanoTime();
                levelWorld.getPlayer().hpDown(0.5f);
            }
        }
    }

    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }
}

package com.veryindyprogrammers.Controllers;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.veryindyprogrammers.Model.LevelWorld;
import com.veryindyprogrammers.Model.Player;
import com.veryindyprogrammers.SabiokaMain;


/**
 * Created by KirillovDaniel on 24.03.2016.
 */
public class OrthographicCameraController implements InputProcessor {
    private OrthographicCamera camera;
    private Player player;
    private LevelWorld stage;

    public OrthographicCameraController(LevelWorld world){
        stage=world;
        this.camera=(OrthographicCamera)world.getCamera();
        this.player=world.getPlayer();
       }

    public void update() {
        if((player.getBody().getPosition().x + SabiokaMain.WORLD_WIDTH / 5f > SabiokaMain.WORLD_WIDTH / 2.2f)
                &&(player.getBody().getPosition().x + SabiokaMain.WORLD_WIDTH / 5f < stage.getColoumns() * SabiokaMain.OUTPUT_TILE_SIZE - SabiokaMain.WORLD_WIDTH / 5*2.2f) )
        {
            camera.position.x = player.getBody().getPosition().x + SabiokaMain.WORLD_WIDTH / 5f;
            camera.position.y = player.getBody().getPosition().y + SabiokaMain.WORLD_HEIGHT / 5f;
        }else {
            //по х левый край
            if (player.getBody().getPosition().x + SabiokaMain.WORLD_WIDTH / 5f <= SabiokaMain.WORLD_WIDTH / 2.2f) {
                camera.position.x = SabiokaMain.WORLD_WIDTH / 2.2f;
                camera.position.y = player.getBody().getPosition().y + SabiokaMain.WORLD_HEIGHT / 5f;
            }
            //по х правый край
            if (player.getBody().getPosition().x + SabiokaMain.WORLD_WIDTH / 5f >= stage.getColoumns() * SabiokaMain.OUTPUT_TILE_SIZE - SabiokaMain.WORLD_WIDTH / 5*2.2f) {
                camera.position.x = stage.getColoumns() * SabiokaMain.OUTPUT_TILE_SIZE - SabiokaMain.WORLD_WIDTH / 5*2.2f;
                camera.position.y = player.getBody().getPosition().y + SabiokaMain.WORLD_HEIGHT / 5f;
            }
        }
        camera.zoom = 0.8f;
        }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}


package com.veryindyprogrammers.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.veryindyprogrammers.Controllers.GameState;
import com.veryindyprogrammers.SabiokaMain;
import com.veryindyprogrammers.View.FontActor;
import com.veryindyprogrammers.View.ImageActor;

import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.util.HashMap;

/**
 * Created by KirillovDaniel on 22.05.2016.
 */
public class PauseScreen implements Screen {
    private Stage stage;
    private ImageActor resumeBtn;
    private ImageActor restartBtn;
    private FontActor paused;

    public PauseScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions){
        restartBtn = new ImageActor(textureRegions.get("restart"), 4.5f, 4f,1.2f, 1.2f);
        restartBtn.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y){
                SabiokaMain.getInstance().restartGame();
                SabiokaMain.getInstance().setState(GameState.RUNNING);
                SabiokaMain.getInstance().showGame();
            }
        });
        resumeBtn = new ImageActor(textureRegions.get("Back button"), 9.8f, 4f, 1.2f, 1.2f);
        resumeBtn.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y){
                SabiokaMain.getInstance().setState(GameState.RUNNING);
                SabiokaMain.getInstance().showGame();
            }
        });
        paused=new FontActor("PAUSE",580.4f, 550.5f, SabiokaMain.getInstance().getMainMenuFont());
        OrthographicCamera camera=new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera),batch);
        stage.addActor(paused);
        stage.addActor(restartBtn);
        stage.addActor(resumeBtn);
    }
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}

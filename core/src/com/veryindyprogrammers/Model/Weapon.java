package com.veryindyprogrammers.Model;

/**
 * Created by KirillovDaniel on 11.05.2016.
 */
public class Weapon {
    private float damage;
    private String name;
    private int price;
    public Weapon(String name,float damage, int price){
        this.damage=damage;
        this.name=name;
        this.price=price;
    }

    public float getDamage() {
        return damage;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}

package com.veryindyprogrammers.Screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.veryindyprogrammers.SabiokaMain;
import com.veryindyprogrammers.View.FontActor;

import java.util.HashMap;

/**
 * Created by KirillovDaniel on 14.04.2016.
 */
public class LoadingScreen implements Screen {
    private SpriteBatch spriteBatch;
    private BitmapFont MainMenuFont;
    private BitmapFont SettingsFont;
    private BitmapFont GameFont;
    private Stage stage;
    private HashMap<String, TextureRegion> textureRegions;
    private String level1,level2;
    private ShapeRenderer shapeRenderer;

    public LoadingScreen(SpriteBatch batch) {
        shapeRenderer = new ShapeRenderer();
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(resolvePath("font/sabFont.otf"));
        //Шрифт для главного меню
        FreeTypeFontGenerator.FreeTypeFontParameter parameterForMainMenu = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameterForMainMenu.size = (int) (80 * SabiokaMain.getInstance().getPpuY_FORTEXT());
        parameterForMainMenu.color = Color.WHITE;
        MainMenuFont = generator.generateFont(parameterForMainMenu);
        //Шрифт для экрана настроек
        FreeTypeFontGenerator.FreeTypeFontParameter parameterForSettings = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameterForSettings.size = (int) (90 * SabiokaMain.getInstance().getPpuY_FORTEXT());
        parameterForSettings.color = Color.WHITE;
        SettingsFont = generator.generateFont(parameterForSettings);
        //шрифт для экрана игры
        FreeTypeFontGenerator.FreeTypeFontParameter parameterForGame = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameterForGame.size = (int) (50 * SabiokaMain.getInstance().getPpuY_FORTEXT());
        parameterForGame.color = Color.WHITE;
        GameFont = generator.generateFont(parameterForGame);

        FontActor fontLoad = new FontActor("LOADING...", 397.5f, 397.5f, SettingsFont);
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);
        stage.addActor(fontLoad);
    }

    public void loadGraphics(){
        Texture tilesAtlas = new Texture(resolvePath("tileset.png"));
        int countx=12;
        int county=4;
        TextureRegion[][] tiles= new TextureRegion[countx][county];
        for(int i=0;i<county; i++)
            for(int j=0; j<countx; j++)
                tiles[j][i] = new TextureRegion(tilesAtlas, j*110, i*110,SabiokaMain.TILE_SIZE,SabiokaMain.TILE_SIZE);

        level1=resolvePath("levels/level1.txt").path();
        level2=resolvePath("levels/level2.txt").path();
        textureRegions=new HashMap<String, TextureRegion>();
        textureRegions.put("Arrow", new TextureRegion(new Texture(resolvePath("arrow.png"))));
        textureRegions.put("Stick", new TextureRegion(new Texture(resolvePath("stick.png"))));
        textureRegions.put("sbBody", new TextureRegion(new Texture(resolvePath("Sabioka.png")), 22, 1, 193, 143));
        textureRegions.put("sbTail", new TextureRegion(new Texture(resolvePath("Sabioka.png")), 0, 144, 53, 16));
        textureRegions.put("sbLeg", new TextureRegion(new Texture(resolvePath("Sabioka.png")), 0, 18, 20, 100));
        textureRegions.put("Play button", new TextureRegion(new Texture(resolvePath("BtnPlayUltra.png"))));
        textureRegions.put("Settings button", new TextureRegion(new Texture(resolvePath("BtnSettings.png"))));
        textureRegions.put("Scores button", new TextureRegion(new Texture(resolvePath("BtnSoresUltra.png"))));
        textureRegions.put("Back color", new TextureRegion(new Texture(resolvePath("cells.png"))));
        textureRegions.put("White color", new TextureRegion(new Texture(resolvePath("cells.png")), 1, 0, 1, 1));
        textureRegions.put("Black color", new TextureRegion(new Texture(resolvePath("cells.png")), 2, 0, 1, 1));
        textureRegions.put("Back button", new TextureRegion(new Texture(resolvePath("btnBack.png"))));
        textureRegions.put("SbSettings", new TextureRegion(new Texture(resolvePath("sbSettings.png"))));
        textureRegions.put("sbStartJumping", new TextureRegion(new Texture(resolvePath("sbJump/sbStartJumping.png"))));
        textureRegions.put("sbJump", new TextureRegion(new Texture(resolvePath("sbJump/sbJump.png"))));
        textureRegions.put("sbStopJumping", new TextureRegion(new Texture(resolvePath("sbJump/sbStopJumping.png"))));
        textureRegions.put("sbRunning1",new TextureRegion(new Texture(resolvePath("sbRun/sbRunning1.png"))));
        textureRegions.put("sbRunning2",new TextureRegion(new Texture(resolvePath("sbRun/sbRunning2.png"))));
        textureRegions.put("UPbtn", new TextureRegion(new Texture(resolvePath("UPbtn.png"))));
        textureRegions.put("DOWNbtn", new TextureRegion(new Texture(resolvePath("DOWNbtn.png"))));
        textureRegions.put("heart", new TextureRegion(new Texture(resolvePath("heart.png"))));
        textureRegions.put("pause", new TextureRegion(new Texture(resolvePath("pauseBtn.png"))));
        textureRegions.put("restart", new TextureRegion(new Texture(resolvePath("restart.png"))));
        textureRegions.put("invisible block", new TextureRegion(tilesAtlas,101,101,1,1));
        textureRegions.put("blockSt", tiles[0][0]);
        textureRegions.put("blockNormal", tiles[1][0]);
        textureRegions.put("fullSt", tiles[2][0]);
        textureRegions.put("fullNormal", tiles[3][0]);
        textureRegions.put("leftSlopeSt", tiles[4][0]);
        textureRegions.put("rightSlopeSt", tiles[5][0]);
        textureRegions.put("leftSlopeNormal", tiles[6][0]);
        textureRegions.put("rightSlopeNormal", tiles[7][0]);
        textureRegions.put("leftConorSt", tiles[8][0]);
        textureRegions.put("rightConorSt", tiles[9][0]);
        textureRegions.put("leftConorNormal", tiles[10][0]);
        textureRegions.put("rightConorNormal", tiles[11][0]);
        textureRegions.put("halfBlockSt", tiles[0][1]);
        textureRegions.put("halfBlockNormal", tiles[1][1]);

        textureRegions.put("achivmentBox",tiles[2][1]);
        textureRegions.put("water", tiles[4][1]);
        textureRegions.put("upWater", tiles[3][1]);
        textureRegions.put("upRightBorder", tiles[5][1]);
        textureRegions.put("downRightBorder", tiles[6][1]);
        textureRegions.put("upButton", tiles[7][1]);
        textureRegions.put("shield", tiles[8][1]);
        textureRegions.put("pins", tiles[9][1]);

        textureRegions.put("blockStR", tiles[0][2]);
        textureRegions.put("blockNormalR", tiles[1][2]);
        textureRegions.put("fullStR", tiles[2][2]);
        textureRegions.put("fullNormalR", tiles[3][2]);
        textureRegions.put("leftSlopeStR", tiles[4][2]);
        textureRegions.put("rightSlopeStR", tiles[5][2]);
        textureRegions.put("leftSlopeNormalR", tiles[6][2]);
        textureRegions.put("rightSlopeNormalR", tiles[7][2]);
        textureRegions.put("leftConorStR", tiles[8][2]);
        textureRegions.put("rightConorStR", tiles[9][2]);
        textureRegions.put("leftConorNormalR", tiles[10][2]);
        textureRegions.put("rightConorNormalR", tiles[11][2]);
        textureRegions.put("halfBlockStR", tiles[0][3]);
        textureRegions.put("halfBlockNormalR", tiles[1][3]);
        textureRegions.put("chestForBC", tiles[2][3]);
        textureRegions.put("chestForIdeas", tiles[3][3]);
        textureRegions.put("leftUpSlopeStR", tiles[4][3]);
        textureRegions.put("rightUpSlopeStR", tiles[5][3]);
        textureRegions.put("leftUpConorStR", tiles[6][3]);
        textureRegions.put("rightUpConorStR", tiles[7][3]);
        textureRegions.put("downBlockStR", tiles[8][3]);
        textureRegions.put("leftBlockStR", tiles[9][3]);
        textureRegions.put("rightBlockStR", tiles[10][3]);

    }

    public static FileHandle resolvePath(String path) {
        String addPath = Gdx.app.getType() == Application.ApplicationType.Desktop?"android/assets/":"";
        return Gdx.files.internal(String.format("%s%s", addPath, path));
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
        if(SabiokaMain.getInstance().getMenuScreen()!=null){
            SabiokaMain.getInstance().showMenu();
            dispose();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void resume() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    public BitmapFont getGameFont() {
        return GameFont;
    }

    public BitmapFont getMainMenuFont(){
        return MainMenuFont;
    }

    public BitmapFont getSettingsFont(){
        return SettingsFont;
    }

    public HashMap<String, TextureRegion> getTextureRegions(){
        return textureRegions;
    }

    public String getLevel1(){
        return level1;
    }

    public String getLevel2() {
        return level2;
    }

}

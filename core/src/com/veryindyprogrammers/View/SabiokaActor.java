package com.veryindyprogrammers.View;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.veryindyprogrammers.SabiokaMain;

import java.util.HashMap;

/**
 * Created by KirillovDaniel on 21.05.2016.
 */
public class SabiokaActor extends Group {
    private ImageActor body, tail, frontRightLeg, frontLeftLeg, backRightLeg, backLeftLeg;
    private float th, tw;
    private long startTime = 0;
    private long intervalTime;
    private float angle;

    public SabiokaActor(HashMap<String, TextureRegion> textureRegions, float x, float y, float w, float h) {
        th = h * 5f;
        tw = w * 5f;
        setPosition(x * SabiokaMain.getInstance().getPpuX(), y * SabiokaMain.getInstance().getPpuY());
        setSize(w * SabiokaMain.getInstance().getPpuX(), h * SabiokaMain.getInstance().getPpuY());
        frontLeftLeg = new ImageActor(textureRegions.get("sbLeg"), x + 0.142f * tw, y, tw * 0.02f, th * 0.082f, "frontLeftLeg");
        frontRightLeg = new ImageActor(textureRegions.get("sbLeg"), x + 0.1565f * tw, y, tw * 0.02f, th * 0.082f, "frontRightLeg");
        backLeftLeg = new ImageActor(textureRegions.get("sbLeg"), x + 0.063f * tw, y, tw * 0.02f, th * 0.082f, "backLeftLeg");
        backRightLeg = new ImageActor(textureRegions.get("sbLeg"), x + 0.0775f * tw, y, tw * 0.02f, th * 0.082f, "backRightLeg");
        tail = new ImageActor(textureRegions.get("sbTail"), x, y + 0.126f * th, tw * 0.053f, th * 0.016f, "tail");
        body = new ImageActor(textureRegions.get("sbBody"), x + 0.039f * tw, y + 0.082f * th, tw * 0.194f, th * 0.143f, "body");
    }

    public void draw(Batch batch, float parentAlpha) {
        intervalTime = System.nanoTime() - startTime;
        if (intervalTime / 100000000 > 1) {
            angle = 45;
            if(intervalTime / 100000000 > 2)
                startTime = System.nanoTime();
        }else{
            angle = -45;
        }
        switch(SabiokaMain.getInstance().getPlayerAction()){
            case MOVE:
                frontLeftLeg.draw(batch, parentAlpha, frontLeftLeg.getWidth()/2, frontLeftLeg.getHeight(), frontLeftLeg.getX(), frontLeftLeg.getY()-frontLeftLeg.getHeight()/4f, angle);
                backLeftLeg.draw(batch, parentAlpha, backLeftLeg.getWidth()/2, backLeftLeg.getHeight(), backLeftLeg.getX(), backLeftLeg.getY()-backLeftLeg.getHeight()/4f, -angle);
                frontRightLeg.draw(batch, parentAlpha, frontRightLeg.getWidth()/2, frontRightLeg.getHeight(), frontRightLeg.getX(), frontRightLeg.getY()-frontRightLeg.getHeight()/4f, angle);
                backRightLeg.draw(batch, parentAlpha, backRightLeg.getWidth()/2, backRightLeg.getHeight(), backRightLeg.getX(), backRightLeg.getY()-backRightLeg.getHeight()/4f, -angle);
                tail.draw(batch, parentAlpha, tail.getWidth(), tail.getHeight()/2,tail.getX(), tail.getY()-frontRightLeg.getHeight()/4f, -45);
                body.draw(batch, parentAlpha,0,0, body.getX(), body.getY()-frontLeftLeg.getHeight()/4f,0);
                break;
            case NONE:
                frontLeftLeg.draw(batch, parentAlpha);
                backLeftLeg.draw(batch, parentAlpha);
                backRightLeg.draw(batch, parentAlpha);
                frontRightLeg.draw(batch, parentAlpha);
                tail.draw(batch, parentAlpha, tail.getWidth(), tail.getHeight()/2, -angle+angle/2);
                body.draw(batch, parentAlpha);
                break;
        }
    }

}

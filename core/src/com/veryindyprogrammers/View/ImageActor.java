package com.veryindyprogrammers.View;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.veryindyprogrammers.SabiokaMain;

/**
 * Created by KirillovDaniel on 19.03.2016.
 */
public class ImageActor extends Actor {
    TextureRegion img;
    ShapeRenderer shapeRenderer = SabiokaMain.getInstance().getShape();

    public ImageActor(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setSize(width * SabiokaMain.getInstance().getPpuX(), height * SabiokaMain.getInstance().getPpuY());
        setPosition(x * SabiokaMain.getInstance().getPpuX(), y * SabiokaMain.getInstance().getPpuY());
    }

    public ImageActor(Texture img, float x, float y) {
        this(new TextureRegion(img), x, y, img.getWidth(), img.getHeight());
    }

    public ImageActor(Texture img, float x, float y, float width, float height) {
        this(new TextureRegion(img), x, y, width, height);
    }

    public ImageActor(TextureRegion img, float x, float y, float width, float height, String name) {
        this(img, x, y, width, height);
        setName(name);
    }

    public ImageActor(TextureRegion img, float x, float y) {
        this(img, x, y, img.getRegionWidth(), img.getRegionHeight());
    }

    public ImageActor(TextureRegion img, float x, float y, boolean needsToResize) {
        float atlasppuX = img.getTexture().getWidth() / SabiokaMain.WORLD_WIDTH;
        float atlasppuY = img.getTexture().getHeight() / SabiokaMain.WORLD_HEIGHT;
        this.img = img;
        setPosition(x * SabiokaMain.getInstance().getPpuX(), y * SabiokaMain.getInstance().getPpuY());
        setSize(img.getRegionWidth() / atlasppuX * SabiokaMain.getInstance().getPpuX(), img.getRegionHeight() / atlasppuY * SabiokaMain.getInstance().getPpuY());
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
        drawRects(batch);
    }

    public void draw(Batch batch, float parentAlpha, float originX, float originY, float angle){
        batch.draw(img, getX(),getY(), originX, originY,
                getWidth(), getHeight(),1,1,angle);
        drawRects(batch);
    }

    public void draw(Batch batch, float parentAlpha, float originX, float originY, float x, float y, float angle){
        batch.draw(img, x,y,
                originX, originY, getWidth(), getHeight(),1,1,angle);
        drawRects(batch);
    }

    private void drawRects(Batch batch){
        if (SabiokaMain.getInstance().isDrawImageActorRounds()) {
            batch.end();
            if (shapeRenderer.getProjectionMatrix() == null)
                shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.RED);
            shapeRenderer.rect(getX() - 1, getY() - 1, getWidth() + 2, getHeight() + 2);
            shapeRenderer.end();
            batch.begin();
        }
    }
}


package com.veryindyprogrammers.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.veryindyprogrammers.SabiokaMain;
import com.veryindyprogrammers.View.FontActor;
import com.veryindyprogrammers.View.ImageActor;

import java.io.FileNotFoundException;
import java.nio.file.FileAlreadyExistsException;
import java.util.HashMap;

/**
 * Created by KirillovDaniel on 12.05.2016.
 */
public class GameOverScreen implements Screen{
    private Stage stage;
    private ImageActor backGround;
    private FontActor fontOver, fontMenu, fontHila;
    private ImageActor invMenu;
    private SpriteBatch batch;
    private HashMap<String,TextureRegion> textureRegions;

    public GameOverScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions){
        this.batch=batch;
        this.textureRegions=textureRegions;
        fontMenu = new FontActor("MAIN MENU",1025.7f,78.9f,SabiokaMain.getInstance().getMainMenuFont());
        fontHila = new FontActor("your time is too big too show it",190f,300f,SabiokaMain.getInstance().getMainMenuFont());
        invMenu = new ImageActor(textureRegions.get("invisible block"),12.3f,0.2f,3f,1f);
        invMenu.addListener(new ClickListener(){public void clicked(InputEvent event, float x,float y){
            SabiokaMain.getInstance().showMenu();
        }});
        backGround =new ImageActor(textureRegions.get("Black color"),0,0, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        fontOver= new FontActor("GAME OVER",473f,395f,SabiokaMain.getInstance().getSettingsFont());
        OrthographicCamera camera=new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage= new Stage(new ScreenViewport(camera),batch);
        stage.addActor(backGround);
        stage.addActor(fontMenu);
        stage.addActor(fontOver);
        stage.addActor(fontHila);
        stage.addActor(invMenu);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        if(SabiokaMain.getInstance().getGameScreen()!=null)
            SabiokaMain.getInstance().restartGame();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }


    @Override
    public void dispose() {

    }
}

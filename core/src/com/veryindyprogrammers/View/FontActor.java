package com.veryindyprogrammers.View;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.veryindyprogrammers.SabiokaMain;

/**
 * Created by KirillovDaniel on 19.03.2016.
 */
public class FontActor extends Actor {
    private String string;
    private BitmapFont font;
    public FontActor(String string, float x, float y, BitmapFont font){
        this.font=font;
        this.string=string;
        setPosition(x* SabiokaMain.getInstance().getPpuX_FORTEXT(),y*SabiokaMain.getInstance().getPpuY_FORTEXT());
    }
    public void draw(Batch batch, float parentAlpha){
        font.draw(batch, string, getX(),getY());
    }
}

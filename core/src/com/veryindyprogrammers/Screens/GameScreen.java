package com.veryindyprogrammers.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.veryindyprogrammers.Controllers.Direction;
import com.veryindyprogrammers.Controllers.GameState;
import com.veryindyprogrammers.Controllers.OrthographicCameraController;
import com.veryindyprogrammers.Controllers.PlayerController;
import com.veryindyprogrammers.Model.LevelWorld;
import com.veryindyprogrammers.SabiokaMain;
import com.veryindyprogrammers.View.FontActor;
import com.veryindyprogrammers.View.HealthScale;
import com.veryindyprogrammers.View.ImageActor;

import java.io.FileNotFoundException;
import java.util.HashMap;


/**
 * Created by KirillovDaniel on 19.03.2016.
 */
public class GameScreen implements Screen {
    private SpriteBatch spriteBatch;
    private Box2DDebugRenderer debugRenderer;
    LevelWorld world;
    private Stage userInterface;
    private Viewport viewport;
    private OrthographicCamera camera;
    private OrthographicCameraController cameraController;
    private ImageActor btnUp, btnDown, pauseBtn, btnUpUI, btnDownUI, pauseBtnUI;
    private HealthScale healthScale;
    private InputMultiplexer multyplexer;
    private PlayerController playerController;
    private String levelName;
    private SpriteBatch batch;
    private HashMap<String, TextureRegion> textureRegions;

    public GameScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions, String level) throws FileNotFoundException {
        levelName = Gdx.files.internal(level).path();
        this.batch = batch;
        this.textureRegions = textureRegions;
        spriteBatch = batch;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        viewport = new StretchViewport(SabiokaMain.WORLD_WIDTH, SabiokaMain.WORLD_HEIGHT, camera);
        world = new LevelWorld(viewport, batch, textureRegions, levelName);
        userInterface = new Stage();
        btnDown = new ImageActor(textureRegions.get("DOWNbtn"), 2.6f, 1f, 1.2f, 1.2f, "btnDown");
        btnUp = new ImageActor(textureRegions.get("UPbtn"), 1.6f, 3f, 1.2f, 1.2f, "btnUp");
        pauseBtn = new ImageActor(textureRegions.get("pause"), 15f, 8f, 0.6f, 0.7f, "btnPause");
        //на UI
        FontActor fontHP = new FontActor("HEALTH", 76f, 670f, SabiokaMain.getInstance().getGameFont());
        FontActor fontLVL = new FontActor(Gdx.files.internal(levelName).nameWithoutExtension(), 620f, 670f, SabiokaMain.getInstance().getGameFont());
        healthScale = new HealthScale(textureRegions.get("heart"), 2.3f, 8f, world);
        pauseBtnUI = new ImageActor(textureRegions.get("pause"), 15f, 8f, 0.6f, 0.7f);
        btnDownUI = new ImageActor(textureRegions.get("DOWNbtn"), 2.6f, 1f, 1.2f, 1.2f);
        btnUpUI = new ImageActor(textureRegions.get("UPbtn"), 1.6f, 3f, 1.2f, 1.2f);
        camera.position.set(world.getPlayer().getX(), world.getPlayer().getY(), 0);
        debugRenderer = new Box2DDebugRenderer();
        userInterface.addActor(btnDown);
        userInterface.addActor(btnUp);
        world.addActor(btnDown);
        world.addActor(btnUp);
        world.addActor(pauseBtn);
        playerController = new PlayerController(userInterface, world);
        cameraController = new OrthographicCameraController(world);
        multyplexer = new InputMultiplexer();
        multyplexer.addProcessor(playerController);
        multyplexer.addProcessor(cameraController);
        multyplexer.addProcessor(world);
        userInterface.addActor(btnDownUI);
        userInterface.addActor(btnUpUI);
        userInterface.addActor(healthScale);
        userInterface.addActor(pauseBtnUI);
        userInterface.addActor(fontHP);
        userInterface.addActor(fontLVL);
        multyplexer.addProcessor(userInterface);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(multyplexer);
    }

    @Override
    public void render(float delta) {
        if (SabiokaMain.getInstance().isNextLevel()) {
            switch (Gdx.files.internal(levelName).nameWithoutExtension().charAt(5)) {
                case '1':
                    SabiokaMain.getInstance().disposeGameScreen();
                    SabiokaMain.getInstance().createGameScreen(batch, textureRegions, SabiokaMain.getInstance().getLevel2());
                    SabiokaMain.getInstance().showGame();
                    return;
            }
            SabiokaMain.getInstance().setNextLevel(false);
        }
        switch (SabiokaMain.getInstance().getState()) {
            case RUNNING:
                Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
                playerController.update();
                cameraController.update();
                world.act(delta);
                world.draw();
                if (SabiokaMain.getInstance().isDrawPhysicsBodiesRounds()) {
                    spriteBatch.begin();
                    debugRenderer.SHAPE_STATIC.set(new Color(0.5f, 0.0f, 1.0f, 1f));
                    debugRenderer.SHAPE_AWAKE.set(new Color(0.5f, 1.0f, 0.0f, 1f));
                    debugRenderer.render(world.getPhysicsWorld(), camera.combined);
                    spriteBatch.end();
                    camera.update();
                }
                userInterface.act(delta);
                userInterface.draw();
                break;
            case PAUSE:
                SabiokaMain.getInstance().showPauseScreen();
                break;
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {
        if (SabiokaMain.getInstance().getState() == GameState.RUNNING)
            SabiokaMain.getInstance().setState(GameState.PAUSE);
        else
            SabiokaMain.getInstance().setState(GameState.RUNNING);
        System.out.println("PAUSED");
    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        if(SabiokaMain.getInstance().getDirection()== Direction.LEFT) {
            SabiokaMain.getInstance().setDirection(Direction.RIGHT);
            //world.getPlayer().getImg().flip(true, false);
        }
        world.getPhysicsWorld().dispose();
        world.dispose();
        userInterface.dispose();
        debugRenderer.dispose();
        SabiokaMain.getInstance().setIsInWater(false);
        SabiokaMain.getInstance().setIsOnPins(false);
        SabiokaMain.getInstance().setPortal(false);
        SabiokaMain.getInstance().setNear(false);
    }

}


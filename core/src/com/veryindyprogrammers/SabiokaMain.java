package com.veryindyprogrammers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.veryindyprogrammers.Controllers.Direction;
import com.veryindyprogrammers.Controllers.GameState;
import com.veryindyprogrammers.Controllers.MovementControlStyle;
import com.veryindyprogrammers.Controllers.PlayerAction;
import com.veryindyprogrammers.Controllers.ToggleState;
import com.veryindyprogrammers.Screens.GameOverScreen;
import com.veryindyprogrammers.Screens.GameScreen;
import com.veryindyprogrammers.Screens.HighscoreScreen;
import com.veryindyprogrammers.Screens.LoadingScreen;
import com.veryindyprogrammers.Screens.MainMenuScreen;
import com.veryindyprogrammers.Screens.PauseScreen;
import com.veryindyprogrammers.Screens.SettingsScreen;
import com.veryindyprogrammers.Screens.ShopScreen;

import java.io.FileNotFoundException;
import java.util.HashMap;

public class SabiokaMain extends Game {

	private float ppuX, ppuY, ppuX_FORTEXT, ppuY_FORTEXT;
	private HashMap<String, TextureRegion> textureRegions;
	private ShapeRenderer shape;
	private SpriteBatch batch;
	private MainMenuScreen menuScreen;
	private GameScreen gameScreen;
	private SettingsScreen settingsScreen;
	private HighscoreScreen highscoreScreen;
	private ShopScreen shopScreen;
	private LoadingScreen loadingScreen;
	private GameOverScreen gameOverScreen;
	private PauseScreen pauseScreen;

	private BitmapFont MainMenuFont;
	private BitmapFont SettingsFont;
	private BitmapFont GameFont;
	private ToggleState toggleState = ToggleState.ON;
	private String level1, level2;
	private boolean isGrounded;
	private boolean portal;
	private boolean isSlopeDown;
	private boolean isSlopeUp;
	private boolean near;
	private boolean isInWater;
	private boolean isOnPins;
	private boolean nextLevel;
	private boolean drawImageActorRounds;
	private boolean drawPhysicsBodiesRounds;
	private Direction direction;
	private PlayerAction playerAction;
	private GameState state;
	public static final MovementControlStyle movementControlStyle = MovementControlStyle.BUTTONS;
	public static final float WORLD_WIDTH = 16f;
	public static final float WORLD_HEIGHT = 9f;
	public static final float WORLD_WIDTH_FORTEXT = 1334f;
	public static final float WORLD_HEIGHT_FORTEXT = 710f;
	public static final int TILE_SIZE = 100;
	public static final float OUTPUT_TILE_SIZE = 0.4f;

	private static SabiokaMain instance = new SabiokaMain();

	public static SabiokaMain getInstance() {
		return instance;
	}

	private SabiokaMain() {
	}

	@Override
	public void create() {
		ppuX = (float) Gdx.graphics.getWidth() / WORLD_WIDTH;
		ppuY = (float) Gdx.graphics.getHeight() / WORLD_HEIGHT;
		ppuX_FORTEXT = (float) Gdx.graphics.getWidth() / WORLD_WIDTH_FORTEXT;
		ppuY_FORTEXT = (float) Gdx.graphics.getHeight() / WORLD_HEIGHT_FORTEXT;
		drawImageActorRounds = true;
		drawPhysicsBodiesRounds = true;
		isGrounded = true;
		portal = false;
		isSlopeDown = false;
		isSlopeUp = false;
		near = false;
		isInWater = false;
		isOnPins = false;
		nextLevel = false;
		direction = Direction.RIGHT;
		state = GameState.RUNNING;
		playerAction = PlayerAction.NONE;
		batch = new SpriteBatch();
		shape = new ShapeRenderer();
		loadingScreen = new LoadingScreen(batch);
		loadingScreen.loadGraphics();
		textureRegions = loadingScreen.getTextureRegions();
		level1 = loadingScreen.getLevel1();
		level2 = loadingScreen.getLevel2();
		MainMenuFont = loadingScreen.getMainMenuFont();
		SettingsFont = loadingScreen.getSettingsFont();
		GameFont = loadingScreen.getGameFont();
		menuScreen = new MainMenuScreen(batch, textureRegions);
		gameOverScreen = new GameOverScreen(batch, textureRegions);
		createGameScreen(batch, textureRegions, level1);
		shopScreen = new ShopScreen(batch, textureRegions);
		settingsScreen = new SettingsScreen(batch, textureRegions);
		highscoreScreen = new HighscoreScreen(batch, textureRegions);
		pauseScreen = new PauseScreen(batch, textureRegions);
		showMenu();
	}

	public void showGame() {
		setScreen(gameScreen);
	}

	public void showPauseScreen() {
		setScreen(pauseScreen);
	}

	public void showLoadingScreen(){
		setScreen(loadingScreen);
	}

	public void showGameOverScreen() {
		setScreen(gameOverScreen);
	}

	public void showMenu() {
		setScreen(menuScreen);
	}

	public void showSettings() {
		setScreen(settingsScreen);
	}

	public void showHighscoreScreen() {
		setScreen(highscoreScreen);
	}

	public void showShopScreen() {
		setScreen(shopScreen);
	}

	public void disposeGameScreen() {
		if(gameScreen!=null)
		gameScreen.dispose();
	}

	public void createGameScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions, String lvl){
		try {
			gameScreen = new GameScreen(batch, textureRegions, lvl);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void restartGame(){
		if(gameScreen!=null)
		gameScreen.dispose();
		createGameScreen(batch, textureRegions,level1);
	}

	@Override
	public void render() {
		super.render();
	}

	public float getPpuX() {
		return ppuX;
	}

	public float getPpuY() {
		return ppuY;
	}

	public HashMap<String, TextureRegion> getTextures() {
		return textureRegions;
	}

	public ShapeRenderer getShape() {
		return shape;
	}

	public BitmapFont getMainMenuFont() {
		return MainMenuFont;
	}

	public BitmapFont getSettingsFont() {
		return SettingsFont;
	}

	public ToggleState getToggleState() {
		return toggleState;
	}

	public void setToggleState(ToggleState toggleState) {
		this.toggleState = toggleState;
	}

	public String getLevel1() {
		return level1;
	}

	public boolean isGrounded() {
		return isGrounded;
	}

	public void setIsGrounded(boolean isGrounded) {
		this.isGrounded = isGrounded;
	}

	public boolean isPortal() {
		return portal;
	}

	public void setPortal(boolean portal) {
		this.portal = portal;
	}

	public BitmapFont getGameFont() {
		return GameFont;
	}

	public void setGameFont(BitmapFont gameFont) {
		GameFont = gameFont;
	}

	public boolean isSlopeDown() {
		return isSlopeDown;
	}

	public void setIsSlopeDown(boolean isSlope) {
		this.isSlopeDown = isSlope;
	}

	public boolean isSlopeUp() {
		return isSlopeUp;
	}

	public void setIsSlopeUp(boolean isSlope) {
		this.isSlopeUp = isSlope;
	}

	public boolean isNear() {
		return near;
	}

	public void setNear(boolean near) {
		this.near = near;
	}

	public boolean isInWater() {
		return isInWater;
	}

	public void setIsInWater(boolean isInWater) {
		this.isInWater = isInWater;
	}

	public GameState getState() {
		return state;
	}

	public void setState(GameState state) {
		this.state = state;
	}

	public String getLevel2() {
		return level2;
	}

	public float getPpuX_FORTEXT() {
		return ppuX_FORTEXT;
	}

	public float getPpuY_FORTEXT() {
		return ppuY_FORTEXT;
	}

	public boolean isOnPins() {
		return isOnPins;
	}

	public void setIsOnPins(boolean isOnPins) {
		this.isOnPins = isOnPins;
	}

	public boolean isNextLevel() {
		return nextLevel;
	}

	public void setNextLevel(boolean nextLevel) {
		this.nextLevel = nextLevel;
	}

	public GameScreen getGameScreen() {
		return gameScreen;
	}

	public MainMenuScreen getMenuScreen() {
		return menuScreen;
	}

	public boolean isDrawImageActorRounds() {
		return drawImageActorRounds;
	}

	public boolean isDrawPhysicsBodiesRounds() {
		return drawPhysicsBodiesRounds;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public PlayerAction getPlayerAction() {
		return playerAction;
	}

	public void setPlayerAction(PlayerAction playerAction) {
		this.playerAction = playerAction;
	}
}

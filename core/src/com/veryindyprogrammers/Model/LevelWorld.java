package com.veryindyprogrammers.Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.veryindyprogrammers.Controllers.SabiokaContactListener;
import com.veryindyprogrammers.SabiokaMain;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by KirillovDaniel on 06.06.2016.
 */
public class LevelWorld extends Stage {
    private World physicsWorld;
    char[][] field;
    private Player player;
    private Vector2 gravity;
    private int coloumns;
    private int rows;
    private String levelName;

    public World getPhysicsWorld() {
        return physicsWorld;
    }

    public LevelWorld(Viewport viewport, SpriteBatch batch, HashMap<String, TextureRegion> textureRegions, String levelName) throws FileNotFoundException {
        super(viewport, batch);
        this.levelName = levelName;
        gravity = new Vector2(0, -10f);
        physicsWorld = new World(gravity, false);
        player = new Player(textureRegions, 2.2f, 4f, physicsWorld);
        player.setSize(SabiokaMain.OUTPUT_TILE_SIZE, SabiokaMain.OUTPUT_TILE_SIZE);
        physicsWorld.setContactListener(new SabiokaContactListener());
        initField(textureRegions, Gdx.files.internal(levelName).path());
        addActor(player);
    }

    public void act(float delta) {
        super.act(delta);
        physicsWorld.step(delta, 6, 4);
    }

    public Player getPlayer() {
        return player;
    }

    private void initField(HashMap<String, TextureRegion> textureRegions, String levelName) throws FileNotFoundException {
        field = createField(Gdx.files.internal(levelName).path());
        coloumns = countColumns(Gdx.files.internal(levelName).path());
        rows = countStrings(Gdx.files.internal(levelName).path());
        System.out.println("_FILENAME_" + Gdx.files.internal(levelName).nameWithoutExtension());
        Box tmpBox = null;
        String tile;
        for (int i = 0; i < countStrings(Gdx.files.internal(levelName).path()); i++)
            for (int j = 0; j < countColumns(Gdx.files.internal(levelName).path()); j++) {
                switch (field[i][j]) {
                    case '0':
                        tmpBox = null;
                        break;
                    case '1':
                        if (isHide(i, j)) tile = "1h";
                        else tile = "1";
                        tmpBox = new Box(textureRegions.get("fullStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case '2':
                        if (isHide(i, j)) tile = "2h";
                        else tile = "2";
                        tmpBox = new Box(textureRegions.get("blockStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case '3':
                        if (isHide(i, j)) tile = "3h";
                        else tile = "3";
                        tmpBox = new Box(textureRegions.get("leftSlopeStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case '4':
                        if (isHide(i, j)) tile = "4h";
                        else tile = "4";
                        tmpBox = new Box(textureRegions.get("rightSlopeStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case '5':
                        if (isHide(i, j)) tile = "5h";
                        else tile = "5";
                        tmpBox = new Box(textureRegions.get("leftConorStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case '6':
                        if (isHide(i, j)) tile = "6h";
                        else tile = "6";
                        tmpBox = new Box(textureRegions.get("rightConorStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case '7':
                        if (isHide(i, j)) tile = "7h";
                        else tile = "7";
                        if (Gdx.files.internal(levelName).nameWithoutExtension().equals("level1") && (field[i - 1][j] == '0' || field[i - 1][j] == '7' || field[i - 1][j] == 'i' || field[i - 1][j] == 'e' || field[i - 1][j] == '8' || field[i - 1][j] == '9') &&
                                (field[i + 1][j] == '0' || field[i + 1][j] == '7' || field[i + 1][j] == 'i' || field[i + 1][j] == 'e' || field[i + 1][j] == '8' || field[i + 1][j] == '9') &&
                                (field[i][j - 1] == '0' || field[i][j - 1] == '7' || field[i][j - 1] == 'i' || field[i][j - 1] == 'e' || field[i][j - 1] == '8' || field[i][j - 1] == '9') &&
                                (field[i][j + 1] == '0' || field[i][j + 1] == '7' || field[i][j + 1] == 'i' || field[i][j + 1] == 'e' || field[i][j + 1] == '8' || field[i][j + 1] == '9')) {
                            tmpBox = new Box(textureRegions.get("halfBlockStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile + "i");
                        } else
                            tmpBox = new Box(textureRegions.get("halfBlockStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case '8':
                        if (isHide(i, j)) tile = "8h";
                        else tile = "8";
                        tmpBox = new Box(textureRegions.get("achivmentBox"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case '9':
                        if (isHide(i, j)) tile = "9h";
                        else tile = "9";
                        tmpBox = new Box(textureRegions.get("chestForBC"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'a':
                        if (isHide(i, j)) tile = "ah";
                        else tile = "a";
                        tmpBox = new Box(textureRegions.get("leftUpSlopeStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'b':
                        if (isHide(i, j)) tile = "bh";
                        else tile = "b";
                        tmpBox = new Box(textureRegions.get("rightUpSlopeStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'c':
                        if (isHide(i, j)) tile = "ch";
                        else tile = "c";
                        tmpBox = new Box(textureRegions.get("leftUpConorStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'd':
                        if (isHide(i, j)) tile = "dh";
                        else tile = "d";
                        tmpBox = new Box(textureRegions.get("rightUpConorStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'e':
                        if (isHide(i, j)) tile = "eh";
                        else tile = "e";
                        tmpBox = new Box(textureRegions.get("chestForIdeas"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'f':
                        if (isHide(i, j)) tile = "fh";
                        else tile = "f";
                        tmpBox = new Box(textureRegions.get("downBlockStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'g':
                        if (isHide(i, j)) tile = "gh";
                        else tile = "g";
                        tmpBox = new Box(textureRegions.get("upWater"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'h':
                        if (isHide(i, j)) tile = "hh";
                        else tile = "h";
                        tmpBox = new Box(textureRegions.get("water"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'i':
                        if (isHide(i, j)) tile = "ih";
                        else tile = "i";
                        if (Gdx.files.internal(levelName).nameWithoutExtension().equals("level1") && (field[i - 1][j] == '0' || field[i - 1][j] == '7' || field[i - 1][j] == 'i' || field[i - 1][j] == 'e' || field[i - 1][j] == '8' || field[i - 1][j] == '9') &&
                                (field[i + 1][j] == '0' || field[i + 1][j] == '7' || field[i + 1][j] == 'i' || field[i + 1][j] == 'e' || field[i + 1][j] == '8' || field[i + 1][j] == '9') &&
                                (field[i][j - 1] == '0' || field[i][j - 1] == '7' || field[i][j - 1] == 'i' || field[i][j - 1] == 'e' || field[i][j - 1] == '8' || field[i][j - 1] == '9') &&
                                (field[i][j + 1] == '0' || field[i][j + 1] == '7' || field[i][j + 1] == 'i' || field[i][j + 1] == 'e' || field[i][j + 1] == '8' || field[i][j + 1] == '9')) {
                            tmpBox = new Box(textureRegions.get("halfBlockStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile + "i");
                        } else
                            tmpBox = new Box(textureRegions.get("halfBlockStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'j':
                        if (isHide(i, j)) tile = "jh";
                        else tile = "j";
                        tmpBox = new Box(textureRegions.get("rightBlockStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'k':
                        if (isHide(i, j)) tile = "kh";
                        else tile = "k";
                        tmpBox = new Box(textureRegions.get("leftBlockStR"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'p':
                        if (isHide(i, j)) tile = "ph";
                        else tile = "p";
                        tmpBox = new Box(textureRegions.get("White color"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'm':
                        if (isHide(i, j)) tile = "mh";
                        else tile = "m";
                        tmpBox = new Box(textureRegions.get("downRightBorder"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'n':
                        if (isHide(i, j)) tile = "nh";
                        else tile = "n";
                        tmpBox = new Box(textureRegions.get("upRightBorder"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'o':
                        if (isHide(i, j)) tile = "oh";
                        else tile = "o";
                        tmpBox = new Box(textureRegions.get("upButton"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 'r':
                        if (isHide(i, j)) tile = "rh";
                        else tile = "r";
                        tmpBox = new Box(textureRegions.get("shield"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                    case 't':
                        if (isHide(i, j)) tile = "th";
                        else tile = "t";
                        tmpBox = new Box(textureRegions.get("pins"), SabiokaMain.OUTPUT_TILE_SIZE * j, SabiokaMain.OUTPUT_TILE_SIZE * i, physicsWorld, tile);
                        break;
                }

                if (tmpBox != null) {
                    tmpBox.setSize(SabiokaMain.OUTPUT_TILE_SIZE, SabiokaMain.OUTPUT_TILE_SIZE);
                    addActor(tmpBox);
                }
            }

    }

    private int countStrings(String fileName) throws FileNotFoundException {
        int result = 0;
        Scanner sc = new Scanner(Gdx.files.internal(fileName).read());
        while (sc.hasNext()) {
            result++;
            sc.nextLine();
        }
        sc.close();
        return result;
    }

    private int countColumns(String fileName) throws FileNotFoundException {
        int result = 0;
        int tmp;
        Scanner sc = new Scanner(Gdx.files.internal(fileName).read());
        while (sc.hasNext()) {
            tmp = sc.nextLine().length();
            //result=tmp > result ? tmp:result;
            if (tmp > result) result = tmp;
        }
        sc.close();
        return result;
    }

    private char[][] createField(String fileName) throws FileNotFoundException {
        char[][] result = new char[countStrings(Gdx.files.internal(levelName).path())][countColumns(Gdx.files.internal(levelName).path())];
        for (int i = 0; i < result.length; i++)
            for (int k = 0; k < result[i].length; k++)
                result[i][k] = '0';
        String tmp;
        Scanner sc = new Scanner(Gdx.files.internal(fileName).read());
        for (int i = 0; i < result.length; i++) {
            tmp = sc.nextLine();
            for (int k = 0; k < tmp.length(); k++) {
                result[i][k] = tmp.charAt(k);
                System.out.print(tmp.charAt(k));
            }
            System.out.println();
        }
        sc.close();
        char[] tmpString;
        for (int i = 0; i < result.length / 2; i++) {
            tmpString = result[i];
            result[i] = result[result.length - i - 1];
            result[result.length - i - 1] = tmpString;
        }
        return result;
    }

    public Actor hit(float x, float y, boolean touchable) {
        return super.hit(x, SabiokaMain.WORLD_HEIGHT * SabiokaMain.getInstance().getPpuY() - y, touchable);
    }

    public int getColoumns() {
        return coloumns;
    }

    public int getRows() {
        return rows;
    }

    private boolean isHide(int i, int j) {
        return Gdx.files.internal(levelName).nameWithoutExtension().equals("level2") && j < 13 && i < 6 && i > 0;
    }

    public String getLevelName() {
        return Gdx.files.internal(levelName).nameWithoutExtension();
    }
}


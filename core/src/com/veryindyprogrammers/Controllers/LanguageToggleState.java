package com.veryindyprogrammers.Controllers;

/**
 * Created by KirillovDaniel on 01.04.2016.
 */
public enum LanguageToggleState {
    EN,
    RU
}

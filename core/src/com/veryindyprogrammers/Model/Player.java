package com.veryindyprogrammers.Model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.veryindyprogrammers.SabiokaMain;
import com.veryindyprogrammers.View.SabiokaActor;

import java.util.HashMap;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.DynamicBody;

/**
 * Created by KirillovDaniel on 08.04.2016.
 */
public class Player extends Actor {
    private SabiokaActor sb;
    private float hp;
    private float money;
    private float armor;
    private float attack;
    private Weapon weapon;
    private Body body;
    private float angle;

    public Player(HashMap<String, TextureRegion> textureRegions, float x, float y, World world) {
        hp = 5f;
        armor = 1f;
        attack = 1.5f;
        sb = new SabiokaActor(textureRegions, x, y, SabiokaMain.OUTPUT_TILE_SIZE, SabiokaMain.OUTPUT_TILE_SIZE);
        sb.setVisible(true);
        /*this.img = textureRegions.get("Sabioka");
        setPosition(x, y);
        setSize(img.getRegionWidth(), img.getRegionHeight());
*/
        BodyDef bodyDef = new BodyDef();
        bodyDef.fixedRotation = false;
        bodyDef.position.x = x;
        bodyDef.position.y = y;
        bodyDef.type = DynamicBody;
        body = world.createBody(bodyDef);
        FixtureDef upPart = new FixtureDef();
        FixtureDef downPart = new FixtureDef();
        downPart.shape = new CircleShape();
        upPart.shape = new PolygonShape();
        ((PolygonShape) upPart.shape).setAsBox(SabiokaMain.OUTPUT_TILE_SIZE / 2 - 0.02f, SabiokaMain.OUTPUT_TILE_SIZE / 4 - 0.02f, new Vector2(0, SabiokaMain.OUTPUT_TILE_SIZE / 4), 0f);
        downPart.shape.setRadius(SabiokaMain.OUTPUT_TILE_SIZE / 2 - 0.04f);
        MassData massData = new MassData();
        massData.mass = 20f;
        body.setMassData(massData);
        downPart.friction = 0f;
        downPart.density = 0f;
        downPart.restitution = 0f;
        upPart.friction = 0f;
        upPart.density = 0f;
        upPart.restitution = 0f;
        body.createFixture(downPart);
        body.getFixtureList().get(0).setUserData("s");
        body.createFixture(upPart);
        downPart.shape.dispose();
        upPart.shape.dispose();
    }

    public void act(float delta) {
        sb.setPosition(body.getPosition().x, body.getPosition().y);
        System.out.println("_PlayerBodyPosition(x,y)___" + body.getPosition().x + "," + body.getPosition().y);
    }

    public void draw(Batch batch, float parentAlpha) {
        sb.draw(batch, parentAlpha);
       // batch.draw(img, getX(), getY(), SabiokaMain.OUTPUT_TILE_SIZE / 2, SabiokaMain.OUTPUT_TILE_SIZE / 2, getWidth(), getHeight(), 1, 1, angle);
    }

    public Body getBody() {
        return body;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public float getHp() {
        return hp;
    }

    public void setHp(float hp) {
        this.hp = hp;
    }

    public void hpDown(float d) {
        if (hp > 0)
            hp = hp - d;
    }

    public void hpUp(float d) {
        hp = hp + d;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

}

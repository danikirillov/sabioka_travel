package com.veryindyprogrammers.View;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.veryindyprogrammers.Controllers.ToggleState;
import com.veryindyprogrammers.SabiokaMain;

/**
 * Created by KirillovDaniel on 19.03.2016.
 */
public class ToggleActor extends Actor {
    TextureRegion imgOn, imgOff, imgArrow;
    private ToggleState state;
    private ImageActor iArrow;

    public void setState(ToggleState state) {
        this.state = state;
    }

    public ToggleState getState() {
        return state;
    }

    public ToggleActor(TextureRegion imgOn, TextureRegion imgOff, float x, float y, float width, float height) {
        setPosition(x * SabiokaMain.getInstance().getPpuX(), y * SabiokaMain.getInstance().getPpuY());
        setSize(width * SabiokaMain.getInstance().getPpuX(), height * SabiokaMain.getInstance().getPpuY());
        this.imgOn = imgOn;
        this.imgOff = imgOff;
    }

    public ToggleActor(ImageActor arrow, float x, float y, float width, float height) {
        setPosition(x * SabiokaMain.getInstance().getPpuX(), y * SabiokaMain.getInstance().getPpuY());
        setSize(width * SabiokaMain.getInstance().getPpuX(), height * SabiokaMain.getInstance().getPpuY());
        this.iArrow = arrow;
        this.imgArrow = arrow.img;
        System.out.println("_ToggleActor___Create");
    }

    public void draw(Batch batch, float parentAlpha) {
        if (imgOn != null && imgOff != null) {
            if (state == ToggleState.ON)
                batch.draw(imgOn, getX(), getY(), getWidth(), getHeight());
            else
                batch.draw(imgOff, getX(), getY(), getWidth(), getHeight());
        } else {
            if (state == ToggleState.ON) {
                batch.draw(imgArrow, iArrow.getX(), iArrow.getY(), iArrow.getWidth() / 2, iArrow.getHeight() / 2, iArrow.getWidth(), iArrow.getHeight(), 1, 1, -180);
                System.out.println("_ToggleActor___ON(Draw)");
            } else {
                batch.draw(imgArrow, iArrow.getX(), iArrow.getY(), iArrow.getWidth() / 2, iArrow.getHeight() / 2, iArrow.getWidth(), iArrow.getHeight(), 1, 1, 0f);
                System.out.println("_ToggleActor___OFF(Draw)");
            }
        }
    }

    public void changeValue() {
        if (state == ToggleState.OFF) {
            state = ToggleState.ON;
            System.out.println("_ToggleActor___ChangeValue(ON)");
        } else {
            state = ToggleState.OFF;
            System.out.println("_ToggleActor___ChangeValue(OFF)");
        }
    }
}

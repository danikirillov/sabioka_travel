package com.veryindyprogrammers.Controllers;

/**
 * Created by KirillovDaniel on 28.03.2016.
 */
public enum ToggleState {
    ON,
    OFF
}

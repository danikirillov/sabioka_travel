package com.veryindyprogrammers.Controllers;

/**
 * Created by KirillovDaniel on 19.03.2016.
 */
public enum PlayerAction {
    STAND,
    MOVE,
    JUMP,
    FALL,
    NONE
}


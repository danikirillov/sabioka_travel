package com.veryindyprogrammers.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.veryindyprogrammers.SabiokaMain;
import com.veryindyprogrammers.View.FontActor;
import com.veryindyprogrammers.View.ImageActor;
import com.veryindyprogrammers.View.SabiokaActor;

import java.util.HashMap;

/**
 * Created by KirillovDaniel on 19.03.2016.
 */
public class MainMenuScreen implements Screen {

    private Stage stage;
    private ImageActor blackRect;
    private ImageActor whiteRect;
    private SabiokaActor sabioka;
    private ImageActor backGround;
    private ImageActor playButton;
    private ImageActor settingsButton;
    private ImageActor highscoreButton;

    public MainMenuScreen(final SpriteBatch batch, final HashMap<String,TextureRegion> textureRegions){
        backGround=new ImageActor(textureRegions.get("Back color"),0,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        blackRect = new ImageActor(textureRegions.get("Black color"),0,0,Gdx.graphics.getWidth(), 0.5f);
        whiteRect = new ImageActor(textureRegions.get("White color"),0,0, Gdx.graphics.getWidth(), 0.5f);
        sabioka = new SabiokaActor(textureRegions,1f,0.5f,3f,3f);
        sabioka.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y){
                SabiokaMain.getInstance().showShopScreen();
            }
        });
        playButton=new ImageActor(textureRegions.get("Play button"),5.5f,2f,5f, 5f);
        playButton.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x,float y){
                SabiokaMain.getInstance().showGame();
            }
        });
        settingsButton=new ImageActor(textureRegions.get("Settings button"),12f, 1f, 2.5f, 2.5f);
        settingsButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y){
                SabiokaMain.getInstance().showSettings();
            }});
        highscoreButton=new ImageActor(textureRegions.get("Scores button"),12f, 5.5f, 2.5f, 2.5f);
        highscoreButton.addListener(new ClickListener(){
            public void clicked(InputEvent event, float x, float y){
                SabiokaMain.getInstance().showHighscoreScreen();
            }
        });

        FontActor font =new FontActor("SABIOKA TRAVEL",54f,650f, SabiokaMain.getInstance().getMainMenuFont());

        OrthographicCamera camera=new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage=new Stage(new ScreenViewport(camera),batch);

        stage.addActor(backGround);
        stage.addActor(blackRect);
        stage.addActor(whiteRect);
        stage.addActor(sabioka);
        stage.addActor(playButton);
        stage.addActor(settingsButton);
        stage.addActor(highscoreButton);
        stage.addActor(font);
    }
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}


package com.veryindyprogrammers.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.veryindyprogrammers.SabiokaMain;
import com.veryindyprogrammers.View.FontActor;
import com.veryindyprogrammers.View.ImageActor;
import com.veryindyprogrammers.View.ToggleActor;

import java.util.HashMap;

/**
 * Created by KirillovDaniel on 19.03.2016.
 */
public class SettingsScreen implements Screen {

    private Stage stage;
    private ImageActor backGround;
    private ImageActor btnBack;
    private ImageActor soundArrow;
    private ImageActor langArrow;
    private ImageActor sbSet;
    private ToggleActor soundToggle;
    private ToggleActor langToggle;

    public SettingsScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions){
        backGround=new ImageActor(textureRegions.get("Back color"),0,0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        soundArrow=new ImageActor(textureRegions.get("Arrow"),3.5f,4.2f,1.5f,1f);
        langArrow = new ImageActor(textureRegions.get("Arrow"),3.7f, 1.2f, 1.5f,1f);
        btnBack=new ImageActor(textureRegions.get("Back button"),13f,1f,1.5f,1.5f);
        btnBack.addListener(new ClickListener(){public void clicked(InputEvent event, float x,float y){SabiokaMain.getInstance().showMenu();}});
        sbSet=new ImageActor(textureRegions.get("SbSettings"), 10f,4f,3.2f,3f);
        soundToggle=new ToggleActor(soundArrow,1f,4.2f,7f,1f);
        soundToggle.setState(SabiokaMain.getInstance().getToggleState());
        soundToggle.addListener(new ClickListener(){public void clicked(InputEvent event, float x,float y){soundToggle.changeValue(); SabiokaMain.getInstance().setToggleState(soundToggle.getState());}});
        langToggle=new ToggleActor(langArrow,1.5f,1.2f,7f,1f);
        langToggle.setState(SabiokaMain.getInstance().getToggleState());
        langToggle.addListener(new ClickListener(){public void clicked(InputEvent event, float x,float y){langToggle.changeValue(); SabiokaMain.getInstance().setToggleState(langToggle.getState());}});
        FontActor fontSettings =new FontActor("SETTINGS",78.9f,552.3f+78.9f, SabiokaMain.getInstance().getSettingsFont());
        FontActor fontVolume= new FontActor("VOLUME",236.8f,433.3f+78.9f,SabiokaMain.getInstance().getSettingsFont());
        FontActor fontLanguage=new FontActor("LANGUAGE",223.8f,196.8f+78.9f,SabiokaMain.getInstance().getSettingsFont());
        FontActor fontON= new FontActor("ON",117.8f,315.6f+78.9f,SabiokaMain.getInstance().getSettingsFont());
        FontActor fontOFF= new FontActor("OFF",512f, 315.6f+78.9f,SabiokaMain.getInstance().getSettingsFont());
        FontActor fontEN = new FontActor("EN",117.8f, 157.8f,SabiokaMain.getInstance().getSettingsFont());
        FontActor fontRU = new FontActor("RU", 552.3f, 157.8f, SabiokaMain.getInstance().getSettingsFont());
        OrthographicCamera camera=new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage=new Stage(new ScreenViewport(camera),batch);
        stage.addActor(backGround);
        stage.addActor(btnBack);
        stage.addActor(sbSet);
        stage.addActor(soundToggle);
        stage.addActor(langToggle);
        stage.addActor(fontON);
        stage.addActor(fontOFF);
        stage.addActor(fontSettings);
        stage.addActor(fontLanguage);
        stage.addActor(fontVolume);
        stage.addActor(fontEN);
        stage.addActor(fontRU);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}


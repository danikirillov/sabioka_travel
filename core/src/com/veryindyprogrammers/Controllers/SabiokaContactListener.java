package com.veryindyprogrammers.Controllers;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.WorldManifold;
import com.veryindyprogrammers.SabiokaMain;

import javax.xml.soap.SAAJMetaFactory;

/**
 * Created by KirillovDaniel on 29.04.2016.
 */
public class SabiokaContactListener implements ContactListener {

    @Override
    public void beginContact(Contact contact) {
        //проверят на земле ли персонаж
        if (contact.getFixtureA().getUserData() != null && contact.getFixtureB().getUserData() != null &&
                (contact.getFixtureA().getUserData().equals("s") || contact.getFixtureB().getUserData().equals("s"))) {
            if (contact.getFixtureA().getUserData().equals("s")
                    && (contact.getFixtureA().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2 > contact.getFixtureB().getBody().getPosition().y)
                    && (contact.getFixtureB().getUserData().equals("2") ||
                    contact.getFixtureB().getUserData().equals("5") ||
                    contact.getFixtureB().getUserData().equals("3") ||
                    contact.getFixtureB().getUserData().equals("4") ||
                    contact.getFixtureB().getUserData().equals("6") ||
                    contact.getFixtureB().getUserData().equals("7") ||
                    contact.getFixtureB().getUserData().equals("7i") ||
                    contact.getFixtureB().getUserData().equals("9") ||
                    contact.getFixtureB().getUserData().equals("t") ||
                    contact.getFixtureB().getUserData().equals("e"))) {
                SabiokaMain.getInstance().setIsGrounded(true);
                System.out.println("ISGROUNDED_(incontactlistener)_state_" + SabiokaMain.getInstance().isGrounded());
            }
            if (contact.getFixtureB().getUserData().equals("s")
                    && (contact.getFixtureB().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2 > contact.getFixtureA().getBody().getPosition().y)
                    && (contact.getFixtureA().getUserData().equals("2") ||
                    contact.getFixtureA().getUserData().equals("5") ||
                    contact.getFixtureA().getUserData().equals("3") ||
                    contact.getFixtureA().getUserData().equals("4") ||
                    contact.getFixtureA().getUserData().equals("6") ||
                    contact.getFixtureA().getUserData().equals("7") ||
                    contact.getFixtureA().getUserData().equals("7i") ||
                    contact.getFixtureA().getUserData().equals("9") ||
                    contact.getFixtureA().getUserData().equals("t") ||
                    contact.getFixtureA().getUserData().equals("e"))) {
                SabiokaMain.getInstance().setIsGrounded(true);
                System.out.println("ISGROUNDED_(incontactlistener)_state_" + SabiokaMain.getInstance().isGrounded());
            }
        }
        //проверят столкнвение с концом уровня
        if (contact.getFixtureA().getUserData() != null && (contact.getFixtureA().getUserData().equals("8"))) {
            contact.setEnabled(true);
            //if (contact.getFixtureB().getBody().getPosition().x > contact.getFixtureA().getBody().getPosition().x + SabiokaMain.OUTPUT_TILE_SIZE / 3
            //      && contact.getFixtureB().getBody().getPosition().y > contact.getFixtureA().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 3) {
            SabiokaMain.getInstance().setNextLevel(true);
            System.out.println("_ALARM!_END_");
            //}
        }
        if (contact.getFixtureB().getUserData() != null && (contact.getFixtureB().getUserData().equals("8"))) {
            contact.setEnabled(true);
            //if (contact.getFixtureA().getBody().getPosition().x > contact.getFixtureB().getBody().getPosition().x + SabiokaMain.OUTPUT_TILE_SIZE / 3
            //     && contact.getFixtureA().getBody().getPosition().y > contact.getFixtureB().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 3) {
            SabiokaMain.getInstance().setNextLevel(true);
            System.out.println("_ALARM!_END_");
            //}
        }
        //немнго ифов для проверки и возвращения видимости(физической) объектам
        if (contact.getFixtureA().getUserData() != null && contact.getFixtureA().getUserData().equals("7i"))
            contact.setEnabled(true);
        if (contact.getFixtureA().getUserData() != null && contact.getFixtureA().getUserData().equals("7"))
            contact.setEnabled(true);
        if (contact.getFixtureA().getUserData() != null && contact.getFixtureA().getUserData().equals("9"))
            contact.setEnabled(true);
        if (contact.getFixtureA().getUserData() != null && contact.getFixtureA().getUserData().equals("e"))
            contact.setEnabled(true);
        if (contact.getFixtureB().getUserData() != null && contact.getFixtureB().getUserData().equals("7"))
            contact.setEnabled(true);
        if (contact.getFixtureB().getUserData() != null && contact.getFixtureB().getUserData().equals("7i"))
            contact.setEnabled(true);
        if (contact.getFixtureB().getUserData() != null && contact.getFixtureB().getUserData().equals("9"))
            contact.setEnabled(true);
        if (contact.getFixtureB().getUserData() != null && contact.getFixtureB().getUserData().equals("e"))
            contact.setEnabled(true);

        //проверяет контакт с порталом
        if (contact.getFixtureA().getUserData() != null && contact.getFixtureA().getUserData().equals("p"))
            SabiokaMain.getInstance().setPortal(true);
        if (contact.getFixtureB().getUserData() != null && contact.getFixtureB().getUserData().equals("p"))
            SabiokaMain.getInstance().setPortal(true);
        //проверяет наличие подставной платформы
        if (contact.getFixtureA().getUserData() != null &&
                (contact.getFixtureA().getUserData().equals("i") || contact.getFixtureA().getUserData().equals("ii"))
                && contact.getFixtureB().getUserData() != null && contact.getFixtureB().getUserData().equals("s")
                && contact.getFixtureA().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 4 < contact.getFixtureB().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2) {
            contact.setEnabled(false);
            contact.getFixtureA().getBody().setLinearVelocity(0f, -4f);
            contact.getFixtureB().getBody().setLinearVelocity(-0.02f, -4f);
        }
        if (contact.getFixtureB().getUserData() != null &&
                (contact.getFixtureB().getUserData().equals("i") || contact.getFixtureB().getUserData().equals("ii"))
                && contact.getFixtureA().getUserData() != null && contact.getFixtureA().getUserData().equals("s")
                && contact.getFixtureB().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 4 < contact.getFixtureA().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2) {
            contact.setEnabled(false);
            contact.getFixtureB().getBody().setLinearVelocity(0f, -4f);
            contact.getFixtureA().getBody().setLinearVelocity(-0.02f, -4f);
        }
        //проверяет поднимается игрок по склону или нет
        if (contact.getFixtureA().getUserData() != null && contact.getFixtureB().getUserData() != null
                && (contact.getFixtureA().getUserData().equals("4")
                || contact.getFixtureB().getUserData().equals("4")
                || contact.getFixtureA().getUserData().equals("3")
                || contact.getFixtureB().getUserData().equals("4"))) {
            if (contact.getFixtureA().getUserData().equals("3")
                    && contact.getFixtureB().getUserData().equals("s")) {
                SabiokaMain.getInstance().setIsSlopeDown(true);
            }
            if (contact.getFixtureB().getUserData().equals("3")
                    && contact.getFixtureA().getUserData().equals("s")) {
                SabiokaMain.getInstance().setIsSlopeDown(true);
            }
            if (contact.getFixtureA().getUserData().equals("4")
                    && contact.getFixtureB().getUserData().equals("s")) {
                SabiokaMain.getInstance().setIsSlopeUp(true);
            }
            if (contact.getFixtureB().getUserData().equals("4")
                    && contact.getFixtureA().getUserData().equals("s")) {
                SabiokaMain.getInstance().setIsSlopeUp(true);
            }
        } else {
            SabiokaMain.getInstance().setIsSlopeDown(false);
            SabiokaMain.getInstance().setIsSlopeUp(false);
        }
        //контакт с шипами, нанесение урона
        if ((contact.getFixtureA().getUserData() != null && contact.getFixtureA().getUserData().equals("t")) ||
                (contact.getFixtureB().getUserData() != null && contact.getFixtureB().getUserData().equals("t"))) {
            if (contact.getFixtureA().getUserData().equals("t")) {
                SabiokaMain.getInstance().setIsOnPins(true);
            }
            if (contact.getFixtureB().getUserData().equals("t")) {
                SabiokaMain.getInstance().setIsOnPins(true);
            }
        } else
            SabiokaMain.getInstance().setIsOnPins(false);
    }

    @Override
    public void endContact(Contact contact) {
        if (contact.getFixtureA().getUserData() != null && (contact.getFixtureA().getUserData().equals("8")))
            SabiokaMain.getInstance().setNear(false);
        if (contact.getFixtureB().getUserData() != null && (contact.getFixtureB().getUserData().equals("8")))
            SabiokaMain.getInstance().setNear(false);

        if (contact.getFixtureA().getUserData() != null && (contact.getFixtureA().getUserData().equals("g") || contact.getFixtureA().getUserData().equals("h"))) {
            contact.setEnabled(true);
            SabiokaMain.getInstance().setIsInWater(false);
        }
        if (contact.getFixtureB().getUserData() != null && (contact.getFixtureB().getUserData().equals("g") || contact.getFixtureB().getUserData().equals("h"))) {
            contact.setEnabled(true);
            SabiokaMain.getInstance().setIsInWater(false);
        }


    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        WorldManifold manifold = contact.getWorldManifold();
        for (int j = 0; j < manifold.getNumberOfContactPoints(); j++) {
            //проверяет есть платформа над персом или нет
            if (contact.getFixtureA().getUserData() != null && contact.getFixtureA().getUserData().equals("7")
                    && contact.getFixtureA().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 4 > contact.getFixtureB().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2)
                contact.setEnabled(false);
            if (contact.getFixtureB().getUserData() != null && contact.getFixtureB().getUserData().equals("7")
                    && contact.getFixtureB().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 4 > contact.getFixtureA().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2)
                contact.setEnabled(false);
            if (contact.getFixtureA().getUserData() != null && contact.getFixtureA().getUserData().equals("7i")
                    && contact.getFixtureA().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 4 > contact.getFixtureB().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2)
                contact.setEnabled(false);
            if (contact.getFixtureB().getUserData() != null && (contact.getFixtureB().getUserData().equals("7i"))
                    && contact.getFixtureB().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 4 > contact.getFixtureA().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2)
                contact.setEnabled(false);
            if (contact.getFixtureA().getUserData() != null && contact.getFixtureA().getUserData().equals("i")
                    && contact.getFixtureA().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 4 > contact.getFixtureB().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2)
                contact.setEnabled(false);
            if (contact.getFixtureB().getUserData() != null && contact.getFixtureB().getUserData().equals("i")
                    && contact.getFixtureB().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 4 > contact.getFixtureA().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2)
                contact.setEnabled(false);
            if (contact.getFixtureA().getUserData() != null && contact.getFixtureA().getUserData().equals("ii")
                    && contact.getFixtureA().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 4 > contact.getFixtureB().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2)
                contact.setEnabled(false);
            if (contact.getFixtureB().getUserData() != null && (contact.getFixtureB().getUserData().equals("ii"))
                    && contact.getFixtureB().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 4 > contact.getFixtureA().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2)
                contact.setEnabled(false);
            if (contact.getFixtureA().getUserData() != null && (contact.getFixtureA().getUserData().equals("e"))
                    && contact.getFixtureA().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 2 > contact.getFixtureB().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2)
                contact.setEnabled(false);
            if (contact.getFixtureB().getUserData() != null && (contact.getFixtureB().getUserData().equals("e"))
                    && contact.getFixtureB().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 2 > contact.getFixtureA().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2)
                contact.setEnabled(false);
            if (contact.getFixtureA().getUserData() != null && (contact.getFixtureA().getUserData().equals("9"))
                    && contact.getFixtureA().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 2 > contact.getFixtureB().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2)
                contact.setEnabled(false);
            if (contact.getFixtureB().getUserData() != null && (contact.getFixtureB().getUserData().equals("9"))
                    && contact.getFixtureB().getBody().getPosition().y + SabiokaMain.OUTPUT_TILE_SIZE / 2 > contact.getFixtureA().getBody().getPosition().y - SabiokaMain.OUTPUT_TILE_SIZE / 2)
                contact.setEnabled(false);
            //проверят столкнвение с концом уровня
            if (contact.getFixtureA().getUserData() != null && (contact.getFixtureA().getUserData().equals("8"))) {
                // contact.setEnabled(false);
                SabiokaMain.getInstance().setNear(true);
            }
            if (contact.getFixtureB().getUserData() != null && (contact.getFixtureB().getUserData().equals("8"))) {
                //contact.setEnabled(false);
                SabiokaMain.getInstance().setNear(true);
            }
            //Проверяет в воде ли персонаж
            if (contact.getFixtureA().getUserData() != null && (contact.getFixtureA().getUserData().equals("g") || contact.getFixtureA().getUserData().equals("h"))) {
                contact.setEnabled(false);
                SabiokaMain.getInstance().setIsInWater(true);
            }

            if (contact.getFixtureB().getUserData() != null && (contact.getFixtureB().getUserData().equals("g") || contact.getFixtureB().getUserData().equals("h"))){
                contact.setEnabled(false);
                SabiokaMain.getInstance().setIsInWater(true);
            }
        }
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
